package main

// this app generates an HTTP(S) GET request to the CWAF API to retrieve alert templates

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

// define a constant for the request URL "http://pib10.cloud-ng.net:31515/v1/alerts/WAF_ALERTS"

const requestUrl = "http://pib10.cloud-ng.net:31515/v1/alerts/WAF_ALERTS"

func main() {
	// a BearToken is required to authenticate with the CWAF API
	// bearer token is stored as the content of "bearer.txt" in the same directory as this app

	// read the bearer token from the file
	bearerFile := "C:\\Users\\sergeyv\\workspace\\code\\labs\\my-golang-playground\\bearer.txt"
	bearer, err := ioutil.ReadFile(bearerFile)

	if err != nil {
		fmt.Println("Error reading bearer token: ", err)
		return
	}

	// remove any leading or trailing whitespace
	bearer = []byte(strings.TrimSpace(string(bearer)))

	// create a new HTTP client
	client := &http.Client{}

	// create a new HTTP request
	req, err := http.NewRequest("GET", requestUrl, nil)

	// specify the bearer token in the request header
	req.Header.Add("Authorization", "Bearer "+string(bearer))
	// req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.0")
	// req.Header.Add("Requestentityids", "fc9cc724-262c-46c2-aaf5-962420511286")
	// 39fe5259-519c-4291-aa14-218cd0ddaa39
	operatorId := "bf9e87be-f6fe-48fd-947e-3b9760c69590"
	req.Header.Add("Requestentityids", operatorId)

	// send the request
	resp, err := client.Do(req)

	if err != nil {
		fmt.Println("Error sending request: ", err)
		return
	}

	// read the response body
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		fmt.Println("Error reading response body: ", err)
		return
	}

	// close the response body
	resp.Body.Close()

	// print the response body
	fmt.Println(string(body))

}
