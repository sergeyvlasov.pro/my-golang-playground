package main

// import and run Func2 from pkg2
import (
	"fmt"

	pkg2 "gitlab.com/sergeyvlasov.pro/my-gloang-playground/pkg2"
)

func main() {
	fmt.Println(pkg2.Func2())
}
